import React, { Component } from 'react'
import { StyleSheet, Text, View, TouchableHighlight, TouchableOpacity, Image, ScrollView, } from 'react-native'
import { DrawerItems, SafeAreaView } from 'react-navigation';






export default class ContentComponent extends Component {
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'purple', flexDirection: 'row' }}>
                <View style={styles.drawerHeader}>
                    <View style={{ flexDirection: 'row', height: 103 }}>
                        <View style={{ marginTop: 53, flexDirection: 'row', marginLeft: 116, }}>
                            <Text style={{ color: 'black', fontFamily: 'ProximaNova-Regular', fontWeight: '700', fontSize: 18 }}>Menu</Text>
                            
                        </View>

                    </View>

                    <View style={{ flex: 3, backgroundColor: '#fff', borderTopStartRadius:8, borderTopEndRadius:8 }}>
                        <TouchableOpacity 
                                      onPress={() => this.props.navigation.navigate('Dashboard')}

                        >
                            <View style={{ flexDirection: 'row' }}>
                                {/* <Image source={require('../images/home_drawer.png')} style={styles.drawerLogo} /> */}
                                <Text style={styles.drawerText}>Home</Text>
                                <TouchableOpacity style={styles.drawerRightIcon} onPress={this.componentHideAndShow}>
                                    {/* <Image source={require('../images/drawer_icon.png')} style={styles.drawerIcon} /> */}

                                </TouchableOpacity>
                            </View>
                        </TouchableOpacity>

                    
                        <TouchableOpacity>
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.drawerBelowText}>About Us</Text>
                                <TouchableOpacity style={styles.drawerRightIcon} onPress={this.componentHideAndShow}>
                                    {/* <Image source={require('../images/drawer_icon.png')} style={styles.drawerIcon} /> */}

                                </TouchableOpacity>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity >
                            <View style={{ flexDirection: 'row' }}>
                                <Text style={styles.drawerBelowText}>Sign Out</Text>
                                
                            </View>
                        </TouchableOpacity>

                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    drawerHeader: {
        backgroundColor: '#bbdefb',
        flex: 1
    },
    drawerLogo: {
        marginLeft: 30,
        marginRight: 12,
        marginTop: 30,
        width: 19,
        height: 12
    },
    drawerText: {
        fontSize: 15,
        fontFamily: 'ProximaNova-Lt',
        fontWeight: '700',
        color: '#4a4a4a',
        marginLeft: 12,
        marginTop: 28,
        textTransform: 'uppercase'
    },
    drawerBelowText: {
        fontSize: 15,
        fontFamily: 'ProximaNova-Lt',
        fontWeight: '700',
        color: '#4a4a4a',
        marginLeft: 10,
        marginTop: 5,
        textTransform: 'uppercase'
    },
    drawerRightIcon: {
        marginTop: 32,
        marginLeft: 236,
        marginRight: 32,
        position: 'absolute'
    },
    drawerIcon: {
        width: 7,
        height: 11,
    }
})



