import {
    createAppContainer,
    createSwitchNavigator,
  } from 'react-navigation';
  import React, { Component } from 'react';
  import { View, TouchableOpacity, Image, Dimensions } from 'react-native';
  import {createDrawerNavigator} from "react-navigation-drawer"
  import bottomTabNavigator from "./bottomtabNavigator"
  import {createStackNavigator} from "react-navigation-stack"
  import ContentComponent from "./drawerComponent"
  import { withNavigation } from 'react-navigation';
  import { DrawerActions } from 'react-navigation-drawer';
 

const DashboardTabNavigator = bottomTabNavigator

const DashboardStackNavigator = createStackNavigator({
  DashboardTabNavigator: {
    screen: DashboardTabNavigator,
    
   
   
    navigationOptions :({navigation}) =>( {
      
      title: 'Welcome',
      headerStyle: {
        
            backgroundColor: '#bbdefb',
          
            height: 63,
            
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontFamily: 'ProximaNova-Semibold',
        fontSize: 20,
       // lineHeight: 20,
        fontWeight: '600',
        color: 'black',
        marginLeft: 75,
        marginRight: 74,
        marginBottom: 23,
        marginTop: 45
      },
      headerLeft:()=>  (
        <TouchableOpacity onPress={() => navigation.dispatch(DrawerActions.openDrawer())}>
        <Image source={require('../assets/images/Hamburger.png')} style={{ height: 15, width: 20, marginLeft:20,marginTop:15}}  />
        </TouchableOpacity>
      ),
     
  })
   }

},);

  const AppDrawerNavigator = createDrawerNavigator({
    Dashboard: {
      screen: DashboardStackNavigator,
    },                           
  }, {
    drawerBackgroundColor: 'green',   
    overlayColor: 'rgba(0, 0, 0, 0.6)',
    contentComponent: ContentComponent,
  
  
  
  });
  

  const AppSwitchNavigator = createSwitchNavigator({
  
    Dashboard:{screen:AppDrawerNavigator}
  
})
  
  const App = createAppContainer(AppSwitchNavigator);
  export default App;
