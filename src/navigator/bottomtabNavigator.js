import { createBottomTabNavigator } from 'react-navigation-tabs';
import Home1 from '../pages/Home.js'
import Profile from '../pages/Profile.js'
import Search from "../pages/Search.js"
import TabBar from "./TabBar.js"


  import React, { Component } from 'react';
  import { View, TouchableOpacity, Image, Dimensions } from 'react-native';
const bottomTabNavigator = createBottomTabNavigator({
    Home: {
      screen: Home1,
      navigationOptions: {
        tabBarLabel: 'Home',
         tabBarIcon: ({ tintColor }) => (
          <View>
            <Image source={require('../assets/images/home.png')} style={{ tintColor: tintColor, height: 25, width: 25,marginBottom:10}} />
      </View>
         )
      }
    },
    Profile: {
      screen:Profile,
      navigationOptions: {
        tabBarLabel: 'Profile',
        tabBarIcon: ({ tintColor }) => (
          <View>
             <Image source={require('../assets/images/upload.png')} style={{ tintColor: tintColor, height: 25, width: 25,marginBottom:10 }} />
          </View>
        )
      }
    },
    Search:{
      screen:Search,
      navigationOptions: {
        tabBarLabel: 'Search',
        tabBarIcon: ({ tintColor }) => (
          <View>
             <Image source={require('../assets/images/search.png')} style={{ tintColor: tintColor, height: 25, width: 25, marginBottom:10 }} />
          </View>
        )
      }
    }

    
 
    },
  {
    tabBarComponent: TabBar,
      initialRouteName: 'Home',
      order: ["Home",'Search' ,'Profile'],
    
      tabBarOptions: {
        activeTintColor: '#64b5f6',
         inactiveTintColor: 'black',
      
     labelStyle: {
       fontSize: 13,
        },
}
  
    })
    export default bottomTabNavigator;
