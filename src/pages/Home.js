import React, { Component } from 'react'
import {
    Text,
   
    StyleSheet,
    
    ScrollView,
    Dimensions
} from 'react-native'


export default class Home extends Component {

    render() {
        return (
            <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={styles.scrollView}>
                    <Text style={styles.text}>This is Home Screen</Text>
               
            </ScrollView>
        )
    }
}
let ScreenHeight = Dimensions.get('window').height;


const styles = StyleSheet.create({
    scrollView: {
    //flexDirection:"row",
    height: ScreenHeight,
    backgroundColor: '#fff',
    
  },
  text:{
      alignSelf:"center",
      marginTop:100,
      fontSize:20,
      
  }
   

})