import React, { Component } from 'react'
import {
    Text,
   
    StyleSheet,
    
    ScrollView,
    Dimensions
} from 'react-native'


export default class Profile extends Component {

    render() {
        return (
            <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={styles.scrollView}>
                    <Text style={styles.text}>This is Profile Screen</Text>
               
            </ScrollView>
        )
    }
}
let ScreenHeight = Dimensions.get('window').height;


const styles = StyleSheet.create({
    scrollView: {

    height: ScreenHeight,
    backgroundColor: '#fff'
  },
  text:{
    alignSelf:"center",
    marginTop:100,
    fontSize:20,
    
}

})